#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

struct Record
{
	int number;
	char letter;
	float id;
};

Record** draw(int size)
{
	srand((unsigned int) time(NULL));

	int number = 0;
	int randRange = 10000;
	int* randArr = new int[randRange];
	for (int i = 0; i < randRange; i++)
	{
		randArr[i] = i + 1;
	}

	Record** records = new Record*[size];
	for (int i = 0; i < size; i++)
	{
		records[i] = new Record;
		records[i]->letter = rand() % 26 + 65;	// letters A-Z
		records[i]->id = 101 + i;

		number = rand() % randRange;
		records[i]->number = randArr[number];
		randArr[number] = randArr[randRange - 1];
		randArr[randRange - 1] = 0;
		randRange--;
	}
	delete[] randArr;

	return records;
}

void clear(Record** records, int size)
{
	for (int i = 0; i < size; i++)
	{
		delete records[i];
	}
	delete[] records;
}

void sort(Record** records, int size)
{
	bool swapped = true;
	Record* temp = nullptr;

	for (int i = 0; i < size; i++)
	{
		swapped = false;

		for (int j = 0; j < size - i - 1; j++)
		{
			if (records[j]->number > records[j + 1]->number)
			{
				temp = records[j];
				records[j] = records[j + 1];
				records[j + 1] = temp;
				swapped = true;
			}
		}

		if (!swapped)
			break;
	}
}

int countLetters(Record** records, int size, char letter)
{
	int counter = 0;

	for (int i = 0; i < size; i++)
	{
		if (records[i]->letter == letter)
			counter++;
	}

	return counter;
}

void printRecords(Record** records, int size, int count)
{
	if (count > size)
	{
		cout << "Zbyt duza liczba elementow do wyswietlenia!" << endl;
		return;
	}

	for (int i = 0; i < count; i++)
	{
		cout << records[i]->number << ' '
			<< records[i]->id << ' '
			<< records[i]->letter << endl;
	}
}

int main()
{
	ifstream inFile;
	inFile.open("inlab01.txt");
	if (!inFile.is_open())
	{
		cout << "Nie udalo sie otworzyc pliku" << endl;
		return -1;
	}

	int size;
	char letter;
	inFile >> size >> letter;
	inFile.close();

	clock_t begin, end;
	double timeSpent;

	// begin timer
	begin = clock();

	Record** records = draw(size);
	sort(records, size);
	int letterCount = countLetters(records, size, letter);
	printRecords(records, size, 20);
	clear(records, size);

	// end timer
	end = clock();
	timeSpent = (double)(end - begin) / CLOCKS_PER_SEC;

	cout << "Liczba wyszukanych znakow " << letter << ": " << letterCount << endl;
	cout << "Czas wykonania: " << timeSpent << endl;

	system("pause");
	return 0;
}
